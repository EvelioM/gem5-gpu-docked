# Getting Started 

Gem5-gpu, like gem5, uses [mercurial](http://mercurial.selenic.com/) for version control. A short guide to mercurial can be found [here](http://mercurial.selenic.com/guide/). A much longer, exhaustive coverage of mercurial can be found in the **hg** book, available online [here](http://hgbook.red-bean.com/). 

Since gem5-gpu builds off of other projects, rather than fork gem5 and GPGPU-Sim, we make heavy use of mercurial patch queues. For more information, see the hg book chapter on patch queues: [http://hgbook.red-bean.com/read/managing-change-with-mercurial-queues.html](http://hgbook.red-bean.com/read/managing-change-with-mercurial-queues.html). 

## Setup

Since we build off of gem5, all the dependencies of gem5 are also dependencies of gem5-gpu. It is suggested that before you try to compile gem5-gpu you compile a copy of gem5 to ensure these baseline dependencies are met. Gem5 dependencies can be found [here](http://gem5.org/Dependencies), and more gem5 information can be found [here](http://gem5.org/). 

One dependency in addition to the gem5 dependencies is a CUDA installation to have access to nvcc and header files. When you have CUDA installed, export the following environment variable used during gem5-gpu build: 

```sh
export CUDAHOME=/path/to/your/cuda/install/cuda
``` 

The versions of software we have used to test gem5-gpu are listed [here](http://goo.gl/S1ts6). Deviations from the tested versions may cause errors. 

Create directory:

```sh
mkdir gem5-gpu
cd gem5-gpu
```
Clone gem5 and gem5-patches

You’ll need the gem5 revision found [here](http://goo.gl/S1ts6). This revision is the latest that is known to work with gem5-gpu. 

```sh
hg qclone http://repo.gem5.org/gem5 -p http://gem5-gpu.cs.wisc.edu/repo/gem5-patches 
cd gem5/ hg update -r <gem5-revision 
hg qpush -a 
cd ../
``` 

Clone GPGPU-Sim and GPGPU-Sim patches (2 separate options)

1. The easy way (recommended): 

```sh
hg qclone http://gem5-gpu.cs.wisc.edu/repo/gpgpu-sim -p http://gem5-gpu.cs.wisc.edu/repo/gpgpu-sim-patches 
cd gpgpu-sim 
hg qpush -a 
cd ../
```

2.  To have full control of GPGPU-Sim (harder): 
 
If you would like to test extensions to the PTX ISA, you may need to modify the Lex+Yacc ISA parser in GPGPU-Sim to enable these extensions. In order to do this, you will need to generate your own ISA parser source code within the GPGPU-Sim code base. Below are instructions for setting up gem5-gpu using the full GPGPU-Sim source to allow for these ISA modifications. You’ll need the “gpgpu-sim-sha” found [here](http://goo.gl/S1ts6). 

**NOTE:** If you set up GPGPU-Sim from the public git server instead of pulling from our repositories, you may not be able to submit review requests to our GPGPU-Sim patches repository 

```sh
git clone git://dev.ece.ubc.ca/gpgpu-sim gpgpu-sim-complete 
cd gpgpu-sim-complete 
git checkout <gpgpu-sim-sha # This updates GPGPU-Sim the latest version that works with gem5-gpu 
cd v3.x 
source setup_environment release 
make 
cd ../../ 
ln -s gpgpu-sim-complete/v3.x/src gpgpu-sim 
cd gpgpu-sim hg init 
cd .hg/ hg clone http://gem5-gpu.cs.wisc.edu/repo/gpgpu-sim-patches patches 
cd ../ 
hg add 
hg ci -m “Initializing GPGPU-Sim Mercurial base” 
hg qpush -a 
cd ../
``` 

Clone gem5-gpu glue code

```sh
hg clone http://gem5-gpu.cs.wisc.edu/repo/gem5-gpu
```

## Build

Below are quick start commands to build and run gem5-gpu with the x86 CPU ISA. For building and running gem5-gpu with the ARM32 CPU ISA, see instructions here: [ARM32 Support](./arm32_support.md). 

```sh
cd gem5 
scons build/X86_VI_hammer_GPU/gem5.opt --default=X86 EXTRAS=../gem5-gpu/src:../gpgpu-sim/ PROTOCOL=VI_hammer GPGPU_SIM=True
```

You can also build using the pre-defined build options files (found in gem5-gpu/build_opts) as follows: 

```sh
cd gem5 
scons build/X86_VI_hammer_GPU/gem5.opt --default=../../../gem5-gpu/build_opts/X86_VI_hammer_GPU EXTRAS=../gem5-gpu/src:../gpgpu-sim/
``` 

## Running gem5-gpu

Gem5-gpu is run the same way as gem5. Here is an example of how to run with syscall emulation mode (requires that you build benchmarks per instructions [here](./benchmarks.md)): 

```sh
build/X86_VI_hammer_GPU/gem5.opt ../gem5-gpu/configs/se_fusion.py -c /path/to/your/benchmarks/rodinia/backprop/gem5_fusion_backprop -o 16
```

The configuration file se_fusion.py constructs a simulated system with CPU and GPU cores, and the benchmark runs on the "bare metal" simulated hardware. To see the full set of options supported by se_fusion.py, run the following: 

```sh
build/X86_VI_hammer_GPU/gem5.opt ../gem5-gpu/configs/se_fusion.py --help
``` 

Syscall emulation mode is limited to simulating benchmarks that call a restricted subset of system calls implemented in gem5. However, gem5-gpu also support full-system simulation, which allows for booting an operating system and running multiprocess and multithreaded workloads. To run gem5-gpu in full-system mode, the following command will boot Linux (assuming you have set up a kernel and disk image per [gem5 instructions](http://gem5.org/Running_gem5#Full_System_.28FS.29_Mode)). We may release disk images in the future, so if you are interested, please email the list. 

```sh
build/X86_VI_hammer_GPU/gem5.opt ../gem5-gpu/configs/fs_fusion.py
``` 
 