FROM ubuntu:14.04

# Ask docker to use bash
SHELL ["/bin/bash", "-c"]

# Set GPU availability
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=8.0"

# Ubuntu needs this
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Madrid
RUN apt-get update -y && apt-get install -y tzdata

# Packages needed
RUN apt-get install -y --no-install-recommends git 
RUN apt-get install -y --no-install-recommends g++-4.4 
RUN apt-get install -y --no-install-recommends gcc-4.4 
RUN apt-get install -y --no-install-recommends gcc-4.6 
RUN apt-get install -y --no-install-recommends g++-4.6 
RUN apt-get install -y --no-install-recommends python 
RUN apt-get install -y --no-install-recommends python-pip 
RUN apt-get install -y --no-install-recommends build-essential 
RUN apt-get install -y --no-install-recommends checkinstall 
RUN apt-get install -y --no-install-recommends libreadline-gplv2-dev 
RUN apt-get install -y --no-install-recommends libncursesw5-dev 
RUN apt-get install -y --no-install-recommends libssl-dev 
RUN apt-get install -y --no-install-recommends libsqlite3-dev 
RUN apt-get install -y --no-install-recommends tk-dev 
RUN apt-get install -y --no-install-recommends libgdbm-dev 
RUN apt-get install -y --no-install-recommends libc6-dev 
RUN apt-get install -y --no-install-recommends libbz2-dev 
RUN apt-get install -y --no-install-recommends scons 
RUN apt-get install -y --no-install-recommends swig 
RUN apt-get install -y --no-install-recommends m4 
RUN apt-get install -y --no-install-recommends autoconf 
RUN apt-get install -y --no-install-recommends automake 
RUN apt-get install -y --no-install-recommends libtool 
RUN apt-get install -y --no-install-recommends curl 
RUN apt-get install -y --no-install-recommends make 
RUN apt-get install -y --no-install-recommends cmake 
RUN apt-get install -y --no-install-recommends unzip 
RUN apt-get install -y --no-install-recommends python-pydot 
RUN apt-get install -y --no-install-recommends flex 
RUN apt-get install -y --no-install-recommends bison 
RUN apt-get install -y --no-install-recommends xutils 
RUN apt-get install -y --no-install-recommends libx11-dev 
RUN apt-get install -y --no-install-recommends libxt-dev 
RUN apt-get install -y --no-install-recommends libxmu-dev 
RUN apt-get install -y --no-install-recommends libxi-dev 
RUN apt-get install -y --no-install-recommends libgl1-mesa-dev 
RUN apt-get install -y --no-install-recommends python-dev 
# RUN apt-get install -y --no-install-recommends python2.7-setuptools // add this for mako
RUN apt-get install -y --no-install-recommends imagemagick 
RUN apt-get install -y --no-install-recommends libpng-dev 
RUN apt-get install -y --no-install-recommends gettext
RUN apt-get install -y --no-install-recommends openssh-client
RUN apt-get install -y --no-install-recommends wget
RUN apt-get install -y --no-install-recommends xutils-dev
RUN apt-get install -y --no-install-recommends zlib1g-dev
RUN apt-get install -y --no-install-recommends libglu1-mesa-dev
RUN apt-get install -y --no-install-recommends doxygen
RUN apt-get install -y --no-install-recommends graphviz
RUN apt-get install -y --no-install-recommends bash-completion

# Needed for git cloning
RUN apt-get install -y \
        ca-certificates \
        && update-ca-certificates

# We will need cuda
WORKDIR /tmp
RUN wget -q --no-check-certificate http://developer.download.nvidia.com/compute/cuda/3_2_prod/toolkit/cudatoolkit_3.2.16_linux_64_ubuntu10.04.run
RUN wget -q --no-check-certificate http://developer.download.nvidia.com/compute/cuda/3_2_prod/sdk/gpucomputingsdk_3.2.16_linux.run

RUN mkdir /usr/local/cuda
RUN /bin/sh cudatoolkit_3.2.16_linux_64_ubuntu10.04.run -- --prefix=/usr/local/cuda
RUN rm -rf cudatoolkit_3.2.16_linux_64_ubuntu10.04.run 

RUN mkdir /sdk
RUN /bin/sh gpucomputingsdk_3.2.16_linux.run -- --prefix=/sdk
RUN mv /sdk/C /usr/local/cuda
RUN rm -rf gpucomputingsdk_3.2.16_linux.run

ENV CUDA_INSTALL_PATH=/usr/local/cuda
ENV CUDAHOME=/usr/local/cuda
ENV PATH="/usr/local/cuda/bin:$PATH"
ENV LD_LIBRARY_PATH=/usr/local/cuda/lib64
ENV LIBRARY_PATH="$LIBRARY_PATH:/usr/local/cuda/C/lib"

WORKDIR /usr/local/cuda/C/common
RUN make 2> /dev/null

# We need mako for mesa compiling
RUN mkdir  /root/.pip
RUN touch  /root/.pip/pip.conf
RUN echo $'[global]\nindex-url=http://pypi.douban.com/simple/' > /root/.pip/pip.conf
RUN pip install mako

#Clone and build gpgpu-sim
WORKDIR /
RUN git clone https://github.com/gem5-gpu/base.git
WORKDIR /base
RUN bash setup_repos.sh

WORKDIR /base/gem5

RUN scons build/X86_VI_hammer_GPU/gem5.opt --default=X86 EXTRAS=../gem5-gpu/src:../gpgpu-sim/ PROTOCOL=VI_hammer GPGPU_SIM=True


WORKDIR /base/gem5
RUN scons build/VI_hammer/gem5.opt --default=ARM EXTRAS=../gem5-gpu/src:../gpgpu-sim/ PROTOCOL=VI_hammer GPGPU_SIM=True

RUN mkdir /base/benchmarks
RUN mkdir /base/m5threads
COPY benchmarks/ /base/benchmarks/
COPY m5threads/ /base/m5threads/
RUN mkdir /codes
COPY codes/ /codes/

WORKDIR /base/m5threads
RUN make
RUN gcc -O3 -static   -c -o pthread.o pthread.c

WORKDIR /base/benchmarks/libcuda
RUN make

RUN chmod +x /codes/compile_file.sh
RUN ln -s /codes/compile_file.sh /usr/bin/compile-for-gem5-gpu

WORKDIR /codes
