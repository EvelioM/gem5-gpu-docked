# Gem5-gpu docked

A docker solution to ease Gem5-gpu setup.

## Using the image

I recommend building the image as such:

```sh
docker build -t $(basename $PWD) .
```

But you are free to name it however you want.

Run it with:

```sh
docker run --rm -it gem5-gpu-docked
```

## Documentation

Here's a formatted version of the gem5-gpu wiki found [here](https://gem5-gpu.cs.wisc.edu/wiki/start). **Take it with a grain of salt**.

[Formatted Gem5-gpu wiki.](./start.md)

**Careful**, this image now takes hours to build because it builds the tests!
