# Current ARM32 Support

Gem5-gpu currently has limited support for ARM32, and even this functionality has not been tested extensively. Specifically, ARM is currently supported in syscall emulation mode, but not full-system mode. The version of gem5 currently used within gem5-gpu only supports ARMv7, which is a 32-bit architecture, so the maximum physical memory size for a simulated system under ARM is 4GB. As further support is developed, the information here may change. 

## Build

To build for the ARM CPU ISA, simply pass the --default=ARM parameter to scons when building: 

```sh
[gem5-gpu] cd gem5 
[gem5-gpu/gem5] scons build/VI_hammer/gem5.opt --default=ARM EXTRAS=../gem5-gpu/src:../gpgpu-sim/ PROTOCOL=VI_hammer GPGPU_SIM=True
``` 

From this point, you can proceed to run gem5-gpu as described in the [quick start guide](./start.md) as long as your benchmarks are built for the ARM CPU ISA, which is described in the [benchmarks guide](./benchmarks.md).