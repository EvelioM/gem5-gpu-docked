# Benchmarks

We are distributing a set of benchmarks that work with gem5-gpu, including: 
- Minimally modified Rodinia workloads 
- Minimally modified Parboil workloads 
- Rodinia no copy benchmarks, which elide memory copies and instead rely on coherence to transfer data between the CPU and GPU 
- Demystifying CUDA benchmarks: microbenchmarks based on the [Demystifying GPU Microarchitecture through Microbenchmarking](http://www.stuffedcow.net/research/cudabmk) paper
- Unit tests 

## Obtaining Benchmarks

```sh
hg clone https://gem5-gpu.cs.wisc.edu/repo/benchmarks/
```

This will create a repository, with [sub-repositories](http://mercurial.selenic.com/wiki/Subrepository), including: 
- common: includes a makefile for compiling CUDA code to run inside gem5-gpu 
- dcmbmk: Demystifying GPU Microarchitecture benchmarks 
- libcuda: A version of libcuda that benchmarks are linked with when being compiled to run in gem5-gpu
- parboil: Parboil benchmarks 
- rodinia: Rodinia benchmarks 
- rodinia-nocopy: Rodinia benchmarks with memcpy api calls elided 
- unittests: A set of simple tests for validating gem5-gpu 

## Compiling Benchmarks 

Gem5-gpu works with cuda 3.1 and 3.2 (the recommended gcc version is 4.4). Please install this on your system before proceeding further. You will also need to install the CUDA SDK, which includes the cutil library that is linked by many of the benchmarks. If you would like to use gem5-gpu ARM32 CPU ISA support, you will also need to obtain a cross-compiler for ARM. We have tested using `arm-linux-gnueabihf-*-4.6 and arm-linux-gnueabihf-*-4.8` (gcc, g++, ld, ar), which can be obtained on Debian-based systems using: 
 
```sh
[gem5-gpu/benchmarks] apt-get install gcc-4.8-arm-linux-gnueabihf-base cpp-4.8-arm-linux-gnueabihf g++-4.8-arm-linux-gnueabihf binutils-arm-linux-gnueabihf
```

Ensure that these tools can be found through your PATH variable (e.g. /usr/bin). 

## Environment Variables 

Define the CUDAHOME environment variable and the path to the NVidia CUDA SDK, so that the makefiles know where they can find your installation of CUDA. Make sure that the CUDA executables (nvcc) are in your path. For example: 
 
 ```sh
export CUDAHOME=<path to your cuda install>/cuda 
export PATH=$PATH:<path to your cuda install>/bin/ 
export NVIDIA_CUDA_SDK_LOCATION=<path to your sdk installation>/C
```
## Compile libcuda

To build libcuda for x86: 
 
 ```sh
[gem5-gpu/benchmarks] cd libcuda 
[gem5-gpu/benchmarks/libcuda] make
```

 To build libcuda for ARM32 (note: Only one ISA can be compiled at a time): 
 
 ```sh
[gem5-gpu/benchmarks] cd libcuda 
[gem5-gpu/benchmarks/libcuda] make ARCH=ARM32
```

## Example of Compiling a Benchmark

To build benchmarks for x86 ISA (requires that you have the x86 libcuda built as described above): 
 
 ```sh
[gem5-gpu/benchmarks] cd rodinia/backprop 
[gem5-gpu/benchmarks/rodinia/backprop] make gem5-fusion
```

 To build benchmarks for ARM ISA (requires that you have the ARM libcuda built as described above): 
 
 ```sh
[gem5-gpu/benchmarks] cd rodinia/backprop 
[gem5-gpu/benchmarks/rodinia/backprop] make gem5-fusion ARCH=ARM32
```

- **Note**: The "gem5-fusion" nomenclature is legacy, and we intend to update it to "gem5-gpu".

## Example of Running a Benchmark

Once you've [built](./start.md) gem5-gpu, you can confirm that your benchmark runs. For example: 
 
 ```sh
build/VI_hammer/gem5.opt ../gem5-gpu/configs/se_fusion.py -c ../benchmarks/rodinia/backprop/gem5_fusion_backprop -o "16"
```
 