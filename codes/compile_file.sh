#!/bin/bash

[[ $# != 1 ]] && { echo -e "\e[1;31mNeeds a single .cu file as input\e[0m"; exit; }

FILE=$1

ext="${FILE##*.}"

[[ "$ext" != "cu" ]] && { echo -e "\e[1;31mNeeds a single .cu file as input\e[0m"; exit; } 

FILENAME=$(basename -s .cu $FILE)

echo -e '\e[1;35mnvcc '$FILENAME'.cu\e[0m'
nvcc -ccbin=/usr/bin/g++-4.6 -Xcompiler -Wall,-O3 -c -arch sm_20 --keep --compiler-options "-fno-strict-aliasing" -DGEM5_FUSION -I/usr/local/cuda/include/ -I/common/inc/ -I/base/gem5/util/m5/ -I/base/benchmarks/libcuda/ -I/C/common/inc/ -L/lib -L/base/m5threads -L/base/benchmarks/libcuda -lcutil_x86_64 -lm5op_x86 -lcutil -DUNIX -O3 $FILENAME.cu -o  $FILENAME
echo -e '\e[1;35mpython sizeHack '$FILENAME'.cu.cpp\e[0m'
python /base/benchmarks/common/sizeHack.py -f $FILENAME.cu.cpp -t sm_20 > /dev/null
echo -e '\e[1;35mg++ '$FILENAME'.cu.cpp -> '$FILENAME'.cu_o\e[0m'
/usr/bin/g++-4.8 -Wl,--no-as-needed -Wall -O3 -c $FILENAME.cu.cpp -o $FILENAME.cu_o
echo -e '\e[1;35mg++ '$FILENAME'.cu_o -> '$FILENAME'\e[0m'
/usr/bin/g++-4.8 -Wl,--no-as-needed -DGEM5_FUSION $FILENAME.cu_o /base/m5threads/pthread.o -L/base/benchmarks/libcuda -lcuda -L/lib -L/base/m5threads -L/base/benchmarks/libcuda -lcutil_x86_64 -lm5op_x86 -O3 -lz -static -static-libgcc -o $FILENAME -lm -lc  -Wl,--whole-archive -Wl,--no-whole-archive
echo -e '\e[1;35mDeleting intermediate files\e[0m'
rm $FILENAME.cpp*
rm $FILENAME.cu.*
rm $FILENAME.cu_*
rm $FILENAME.cudafe*
rm $FILENAME.hash
rm $FILENAME.ptx
rm $FILENAME.fatbin.c
rm $FILENAME.sm*
echo -e '\e[1;32mDone\e[0m'

